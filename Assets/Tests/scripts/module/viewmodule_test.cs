﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IcecreamView;

public class viewmodule_test : IC_AbstractModule
{

    [BindUIPath("Title")]
    private Text _title;

    [BindUIEvent(1)]
    public void SetText(EventArg rEventArg)
    {
        this._title.text = rEventArg.GetValue<string>(0);
    }

    [BindUIButton("TestBtn")]
    private void OnBtnClick()
    {
        Debug.LogError("测试Btn");
    }
    
    [BindUIButton("ButtonGame")]
    private void OnEventBtnClick()
    {
        //发送事件消息
        this.ViewConnector.SendEvent(1 , "测试Event");
    }

}